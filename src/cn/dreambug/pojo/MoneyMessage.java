package cn.dreambug.pojo;

import java.io.Serializable;

public class MoneyMessage implements Serializable{
/**
 * 
 */
	private static final long serialVersionUID = 1L;
	private String money;
	private String remarks;
	private String summary;
	private String timeStamp;
	private String url;

	public String getMoney() {
		return money;
	}

	public void setMoney(String money) {
		this.money = money;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String toString() {
		String content = "";

		if (money != null && timeStamp != "") {
			content = content + "***************收到转账******************\n";
			content = content + "* 金额：" + money + "\t||备注：" + remarks + "\t\n";
			content = content + "* 汇总：" + summary + "\t||时间：" + timeStamp + "\t\n";
			content = content + "* URL：" + url + "\t\t*\n";
			content = content + "*****************************************\n";
		} else {
			content = content + "***************收到转账******************\n";
			content = content + "* 金额：" + money + "\t||顾客：" + remarks + "\t\n";
			content = content + "* 汇总：" + summary + "\t||时间：" + timeStamp + "\t\n";
			content = content + "* 单号：" + url + "\t\t*\n";
			content = content + "*****************************************";
		}
		return content;
	}
}
