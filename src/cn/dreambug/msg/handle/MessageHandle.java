package cn.dreambug.msg.handle;

import blade.kit.json.JSONObject;
/**
 * 消息处理
 * @author LinWen
 *
 */
public interface MessageHandle {
	/**
	 *  消息处理方法
	 * @return
	 */
	public boolean handleMessage(JSONObject msg);
}
