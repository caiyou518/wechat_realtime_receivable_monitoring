package cn.dreambug.msg.handle;

import java.util.ArrayList;
import java.util.List;

import blade.kit.json.JSONObject;
import cn.dreambug.util.MessageUtil;
import cn.dreambug.weixin.Session;
import cn.dreambug.weixin.UserControl;




public class TextMessageHandle extends BaseMessageHandle{
	/**
	 * 消息处理器列表
	 */
	public List<MessageHandle> handleList;
	
	public TextMessageHandle(Session session, UserControl control) {
		super(session, control);
		this.handleList = new ArrayList<MessageHandle>();
	}
	
	@Override
	public boolean handleMessage(JSONObject msg) {
		// TODO 自动生成的方法存根
		int msgType = MessageUtil.getMsgType(msg);
		if(msgType==1){
			for(MessageHandle mh:this.handleList){ //消息处理
				if(mh.handleMessage(msg)){
					return true;
				}
			}
			return false;
		}else{
			return false;
		}
		
	}
	
}
