package cn.dreambug.msg.handle;

import blade.kit.json.JSONObject;
import cn.dreambug.weixin.Session;
import cn.dreambug.weixin.UserControl;


public abstract class BaseMessageHandle implements MessageHandle{
	private Session session ;
	private UserControl control;
	
	public BaseMessageHandle(Session session,UserControl control){
		this.session = session;
		this.control=control;
	}
	
	
	
	public Session getSession() {
		return session;
	}



	public void setSession(Session session) {
		this.session = session;
	}



	public UserControl getControl() {
		return control;
	}



	public void setControl(UserControl control) {
		this.control = control;
	}



	@Override
	public abstract boolean handleMessage(JSONObject msg) ;
	

}
