package cn.dreambug.util;

import java.util.Base64;

public class StringTool {
	
	/**
	 * 截取字符串
	 * @param str 被截取的字符串
	 * @param strStart 开始字符串
	 * @param strEnd 结束字符串
	 * @return
	 */
	public static String subString(String str, String strStart, String strEnd) {
		int strStartIndex = str.indexOf(strStart);
		if (strStartIndex == -1)
			return null;
		strStartIndex += strStart.length();
		int strEndIndex = str.indexOf(strEnd, strStartIndex + 1);
		if (strEndIndex == -1)
			return null;
        return str.substring(strStartIndex, strEndIndex);
	}
	
	/**
	 * Base64解码
	 * @param arg
	 * @return
	 */
	public static byte[] Base64Decoder(String arg) {
		return Base64.getDecoder().decode(arg);
	}
	
	/**
	 *  Base64编码
	 * @param arg
	 * @return
	 */
	public static String Base64(byte[] arg) {
		return Base64.getEncoder().encodeToString(arg);
	}
	
}
