package cn.dreambug.weixin;

import java.util.ArrayList;
import java.util.List;

import blade.kit.json.JSONArray;
import blade.kit.json.JSONObject;
import cn.dreambug.messageQ.Sender;
import cn.dreambug.msg.handle.MessageHandle;
import cn.dreambug.pojo.MoneyMessage;
import cn.dreambug.util.SmallUtil;
import cn.dreambug.util.StringTool;


public class UserControl {
	@SuppressWarnings("unused")
	private Session session;
	
	/**
	 * 消息处理器列表
	 */
	public List<MessageHandle> handleList;
	
	public UserControl(Session userSession) {
		this.session=userSession;
		this.init();
	}
	
	/**
	 * control初始化
	 */
	private void init() {
		this.handleList = new ArrayList<MessageHandle>();
	}


	/**
	 * 获取最新消息
	 */
	public void handleMsg(JSONObject data){
		if(null == data){
			return;
		}
		
		JSONArray AddMsgList = data.getJSONArray("AddMsgList");
		System.out.println("AddMsgList:" + AddMsgList.size());
		for(int i=0,len=AddMsgList.size(); i<len; i++){
			JSONObject msg = AddMsgList.getJSONObject(i);
			int msgType = msg.getInt("MsgType", 0);
			String content = msg.getString("Content");
			System.out.println(content);
			
			if (msgType == 49) {
				MoneyMessage moneyMessage = new MoneyMessage();
				String money = StringTool.subString(content,"收款金额：￥","<br/");
				String remarks = StringTool.subString(content,"付款方备注：","<br/");
				String summary = StringTool.subString(content,">汇总：","<br/");
				String timeStamp = StringTool.subString(content,"timestamp=","&amp");
				String URL = StringTool.subString(content,"&lt;url&gt;&lt;![CDATA[","]]");
				timeStamp = SmallUtil.timeStamp2Date(timeStamp,null);
				
				if(money != null && timeStamp != ""){
					moneyMessage.setMoney(money);
					moneyMessage.setRemarks(remarks);
					moneyMessage.setSummary(summary);
					moneyMessage.setTimeStamp(timeStamp);
					moneyMessage.setUrl(URL);
				}else if(money != null && timeStamp == ""){
					remarks = StringTool.subString(content,"顾客昵称：","<br/");
					summary = StringTool.subString(content,"截止该单，","，可点击查看详情");
					timeStamp = StringTool.subString(content,"买单时间：","<br/");
					timeStamp = SmallUtil.timeStamp2Date(timeStamp,null);
					URL = StringTool.subString(content,"交易单号：","<br/");
					
					moneyMessage.setMoney(money);
					moneyMessage.setRemarks(remarks);
					moneyMessage.setSummary(summary);
					moneyMessage.setTimeStamp(timeStamp);
					moneyMessage.setUrl(URL);
				}
				try{
					Sender.sendMessage(moneyMessage);
				}catch (Exception e) {
					e.printStackTrace();
				}
				
			}
		}
	}

}
