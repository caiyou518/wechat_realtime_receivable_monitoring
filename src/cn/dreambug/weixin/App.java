package cn.dreambug.weixin;

import java.awt.EventQueue;
import java.io.File;

import javax.swing.UIManager;

import blade.kit.DateKit;
import blade.kit.http.HttpRequest;
import blade.kit.json.JSONObject;
import cn.dreambug.messageQ.Recv;



/**
 * APP 主类
 * @author LinWen
 *
 */
public class App {
	
	private Session session = new Session();
	private UserControl userControl = new UserControl(session);
	
	public QRCodeFrame qrCodeFrame ;
	
	
	public App(){
		System.setProperty("jsse.enableSNIExtension", "false");
	}
	
	
	/**
	 * Show二维码窗口
	 * @return void
	 */
	public void showQrCode() {
		String url = "https://login.weixin.qq.com/qrcode/" + session.uuid;
		
		final File output = new File("temp.jpg");
		
		HttpRequest.post(url, true, 
				"t", "webwx", 
				"_" , DateKit.getCurrentUnixTime())
				.receive(output);

		if(null != output && output.exists() && output.isFile()){
			EventQueue.invokeLater(new Runnable() {
				public void run() {
					try {
						UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
						qrCodeFrame = new QRCodeFrame(output.getPath());
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			});
		}
	}
	/**
	 * Close二维码窗口
	 */
	private void closeQrWindow() {
		qrCodeFrame.dispose();
	}
	
	public static void startActive() {
		App app = new App();
		Recv.messageRecvInit();
		String uuid = app.session.getUUID();
		if(uuid == null){
			System.err.println("[*] uuid获取失败！");
		}else{
			app.showQrCode();
			if(!app.session.login()){
				System.out.println("[*] 微信登录失败");
				return;
			}else{
				app.closeQrWindow();
				
				System.out.println("[*] 微信登录成功！");
				
				if(!app.session.wxInit()){
					System.out.println("[*] 微信初始化失败");
					return;
				}
				app.listenMsg();


			}
		}

	}

	/**
	 * 消息监听
	 */
	private void listenMsg() {
		// TODO Auto-generated method stub
		new Thread(new Runnable() {
			@Override
			public void run() {
				System.out.println("[*] 进入消息监听模式..");

				boolean is_exit = false;

				while(!is_exit){
					
					int[] arr = session.syncCheck();
					
					//System.out.println("[*] retcode="+arr[0]+",selector="+arr[1]);
					
					if(arr[0] == 1100){
						//用户在手机上登出微信。
						System.out.println("在手机上已经退出微信。");
						is_exit = true;
					}
					
					if(arr[0] == 0){
						if(arr[1] == 2){
							JSONObject data = session.webwxsync();
							userControl.handleMsg(data);
						} else if(arr[1] == 6){
							JSONObject data = session.webwxsync();
							userControl.handleMsg(data);
						} else if(arr[1] == 7){
							JSONObject data = session.webwxsync();
							userControl.handleMsg(data);
						} else if(arr[1] == 3){
						} else if(arr[1] == 0){
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					} else {
						try {
							Thread.sleep(1000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}, "listenMsg").start();
	}
}
